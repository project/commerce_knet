# Commerce KNET

## CONTENTS OF THIS FILE

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * How it works
 * Maintainers

## INTRODUCTION

**Commerce KNET** is [Drupal Commerce](https://drupal.org/project/commerce)
module that integrates the [KNET](https://www.knet.com.kw/) payement
gateway into your Drupal Commerce shop.


## REQUIREMENTS

This module requires the following:
* Submodules of Drupal Commerce package (https://drupal.org/project/commerce)
  - Commerce core
  - Commerce Payment (and its dependencies)
* KNET account (test or live) (https://www.knet.com.kw/)


## INSTALLATION

* Install normally as other modules are installed. For Support:
   https://www.drupal.org/docs/8/extending-drupal/installing-contributed-modules

## CONFIGURATION

* Create a new Checkout.com payment gateway.
  Administration > Commerce > Configuration > Payment gateways > Add payment gateway
  KNET settings available. Check the form for detailed information on the
  available configurations.

  It is recommended to enter test credentials and then override these with live
  credentials in settings.php. This way, live credentials will not be stored in
  the db.

# TEST CARD DETAILS

Note: This can change over time.

1. Captured (Approved): card#: 8888880000000001, Expiry: 09/25, Pin: any pin
2. Not Captured (Declined): card#: 8888880000000001, Expiry: any other expiry, Pin: any pin
3. Canceled: press on the Cancel button on the KNET payment page.

Please make sure to select *KNET Test Card [KNET 1]* from the dropdown list on the Knet payment page.
