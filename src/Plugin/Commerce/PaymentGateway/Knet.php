<?php

namespace Drupal\commerce_knet\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the KNET Off-site Redirect payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "knet",
 *   label = "KNET",
 *   display_label = "KNET",
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_knet\PluginForm\KnetPaymentOffsiteForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "knet",
 *   },
 *   requires_billing_information = FALSE,
 * )
 */
class Knet extends OffsitePaymentGatewayBase implements KnetInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $configuration = [
      'endpoint_live' => 'https://kpay.com.kw/kpg/PaymentHTTP.htm',
      'endpoint_test' => 'https://kpaytest.com.kw/kpg/PaymentHTTP.htm',
      'tranportal_id' => '',
      'tranportal_password' => '',
      'terminal_resource_key' => '',
      'udf5' => '[order_number]',
    ] + parent::defaultConfiguration();

    return $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['endpoint_live'] = [
      '#title' => $this->t('Endpoint for Live mode'),
      '#type' => 'textfield',
      '#description' => $this->t('Current KNET endpoint for live: https://kpay.com.kw/kpg/PaymentHTTP.htm'),
      '#default_value' => $this->configuration['endpoint'] ?? 'https://kpay.com.kw/kpg/PaymentHTTP.htm',
      '#required' => TRUE,
    ];

    $form['endpoint_test'] = [
      '#title' => $this->t('Endpoint for Test mode'),
      '#type' => 'textfield',
      '#description' => $this->t('Current KNET endpoint for test mode: https://kpaytest.com.kw/kpg/PaymentHTTP.htm'),
      '#default_value' => $this->configuration['endpoint'] ?? 'https://kpaytest.com.kw/kpg/PaymentHTTP.htm',
      '#required' => TRUE,
    ];

    $form['tranportal_id'] = [
      '#title' => $this->t('TRANPORTAL ID'),
      '#type' => 'textfield',
      '#description' => $this->t('Information received from KNET.'),
      '#default_value' => $this->configuration['tranportal_id'],
      '#required' => TRUE,
    ];

    $form['tranportal_password'] = [
      '#title' => $this->t('TRANPORTAL PASSWORD'),
      '#type' => 'textfield',
      '#description' => $this->t('Information received from KNET.'),
      '#default_value' => $this->configuration['tranportal_password'],
      '#required' => TRUE,
    ];

    $form['terminal_resource_key'] = [
      '#title' => $this->t('TERMINAL RESOURCE KEY'),
      '#type' => 'textfield',
      '#description' => $this->t('Information received from KNET.'),
      '#default_value' => $this->configuration['terminal_resource_key'],
      '#required' => TRUE,
    ];

    $tokens = [];
    $tokens[] = $this->t('Following tokens are available for dynamic replacements in UDF data.');
    $tokens[] = $this->t('UDF1, UDF2, UDF3 and UDF4 are used for internal validations.');
    $tokens[] = $this->t('@token: Current customer id (Drupal user id) will be set here. It will be zero for anonymous users.', ['@token' => '[user_id]']);
    $tokens[] = $this->t('@token: Current customer email (Drupal user mail) will be set here. It will be taken from mail entered in order for anonymous users.', ['@token' => '[user_mail]']);
    $tokens[] = $this->t('@token: Order Number (friendly one).', ['@token' => '[order_number]']);
    $tokens[] = $this->t('@token: Order ID (technical id).', ['@token' => '[order_id]']);
    $tokens[] = $this->t('@token: Language code in which transaction is done.', ['@token' => '[langcode]']);

    $form['tokens'] = [
      '#type' => 'markup',
      '#markup' => implode('<br>', $tokens),
    ];

    $form['udf5'] = [
      '#title' => $this->t('UDF5'),
      '#type' => 'textfield',
      '#description' => $this->t('Please enter text with tokens which will be set dynamically for each transaction.'),
      '#default_value' => $this->configuration['udf5'] ?? '[order_id]',
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['endpoint_live'] = $values['endpoint_live'];
      $this->configuration['endpoint_test'] = $values['endpoint_test'];
      $this->configuration['tranportal_id'] = $values['tranportal_id'];
      $this->configuration['tranportal_password'] = $values['tranportal_password'];
      $this->configuration['terminal_resource_key'] = $values['terminal_resource_key'];
      $this->configuration['udf5'] = $values['udf5'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaymentInstructions(PaymentInterface $payment) {
    $additional_data = $payment->getAdditionalData();

    $instructions = [
      '#type' => 'markup',
      '#markup' => $this->t('Transaction ID: @transaction_id, Payment ID: @payment_id, Status: @status', [
        '@transaction_id' => $additional_data['tranid'] ?? $payment->getRemoteId(),
        '@payment_id' => $additional_data['paymentid'] ?? '',
        '@status' => $payment->getRemoteState(),
      ]),
    ];

    return $instructions;
  }

}
