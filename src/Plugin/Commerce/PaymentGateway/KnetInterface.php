<?php

namespace Drupal\commerce_knet\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\HasPaymentInstructionsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface;

/**
 * Provides the interface for the KNET payment gateway.
 */
interface KnetInterface extends OffsitePaymentGatewayInterface, HasPaymentInstructionsInterface {

}
