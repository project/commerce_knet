<?php

namespace Drupal\commerce_knet\Toolkit;

use Drupal\commerce_knet\Helper\SecureText;

/**
 * Class KnetToolKit.
 */
class KnetToolKit {

  const KNET_ACTION_PURCHASE = 1;

  /**
   * Language mappings.
   *
   * @var array
   */
  protected static $languageMapping = [
    'en' => 'USA',
    'ar' => 'AR',
  ];

  /**
   * Currency code.
   *
   * For Kuwait it is 414.
   *
   * @var int
   */
  protected $currencyCode = 414;

  /**
   * Amount.
   *
   * @var float
   */
  protected $amount;

  /**
   * TRANPORTAL id.
   *
   * @var mixed
   */
  protected $tranportalId = NULL;

  /**
   * TRANPORTAL password.
   *
   * @var string
   */
  protected $tranportalPassword;

  /**
   * Terminal resource key.
   *
   * @var string
   */
  protected $terminalResourceKey;

  /**
   * K-Net PG url.
   *
   * @var string
   */
  protected $knetUrl;

  /**
   * Purchase Action.
   *
   * @var int
   */
  protected $action = self::KNET_ACTION_PURCHASE;

  /**
   * Language Code.
   *
   * @var string
   */
  protected $language;

  /**
   * Tracking ID.
   *
   * @var string|null
   */
  protected $trackId = NULL;

  /**
   * User defined data 1.
   *
   * @var string
   */
  protected $udf1 = '';

  /**
   * User defined data 2.
   *
   * @var string
   */
  protected $udf2 = '';

  /**
   * User defined data 3.
   *
   * @var string
   */
  protected $udf3 = '';

  /**
   * User defined data 4.
   *
   * @var string
   */
  protected $udf4 = '';

  /**
   * User defined data 5.
   *
   * @var string
   */
  protected $udf5 = '';

  /**
   * Response URL.
   *
   * @var string
   */
  protected $responseUrl = '';

  /**
   * Error URL.
   *
   * @var string
   */
  protected $errorUrl = '';

  /**
   * Function to set amount.
   *
   * @param float $amount
   *   Amount.
   */
  public function setAmount($amount) {
    $this->amount = $amount;
  }

  /**
   * Function to set purchase action.
   *
   * @param int $action
   *   Purchase action id.
   */
  public function setAction(int $action) {
    $this->action = $action;
  }

  /**
   * Function to set Currency code.
   *
   * @param int $currency_code
   *   Currency code.
   */
  public function setCurrencyCode(int $currency_code) {
    $this->currencyCode = $currency_code;
  }

  /**
   * Set tranportal id.
   *
   * @param string $tranportal_id
   *   Tranportal id.
   */
  public function setTranportalId($tranportal_id) {
    $this->tranportalId = $tranportal_id;
  }

  /**
   * Set tranportal password.
   *
   * @param string $tranportal_password
   *   Tranportal password.
   */
  public function setTranportalPassword($tranportal_password) {
    $this->tranportalPassword = $tranportal_password;
  }

  /**
   * Set terminal resource key.
   *
   * @param string $terminal_resource_key
   *   Terminal resource key.
   */
  public function setTerminalResourceKey($terminal_resource_key) {
    $this->terminalResourceKey = $terminal_resource_key;
  }

  /**
   * Set the K-net Url.
   *
   * @param string $knet_url
   *   K-Net url.
   */
  public function setKnetUrl($knet_url) {
    $this->knetUrl = $knet_url;
  }

  /**
   * Set the tracking id.
   *
   * @param string $trackId
   *   Tracking ID.
   */
  public function setTrackId(string $trackId) {
    $this->trackId = $trackId;
  }

  /**
   * Set User defined data 1.
   *
   * @param string $udf1
   *   User defined data 1.
   */
  public function setUdf1(string $udf1) {
    $this->udf1 = $udf1;
  }

  /**
   * Set User defined data 2.
   *
   * @param string $udf2
   *   User defined data 2.
   */
  public function setUdf2(string $udf2) {
    $this->udf2 = $udf2;
  }

  /**
   * Set User defined data 3.
   *
   * @param string $udf3
   *   User defined data 3.
   */
  public function setUdf3(string $udf3) {
    $this->udf3 = $udf3;
  }

  /**
   * Set User defined data 4.
   *
   * @param string $udf4
   *   User defined data 4.
   */
  public function setUdf4(string $udf4) {
    $this->udf4 = $udf4;
  }

  /**
   * Set User defined data 5.
   *
   * @param string $udf5
   *   User defined data 5.
   */
  public function setUdf5(string $udf5) {
    $this->udf5 = $udf5;
  }

  /**
   * Set Response URL.
   *
   * @param string $response_url
   *   Response URL.
   */
  public function setResponseUrl(string $response_url) {
    $this->responseUrl = $response_url;
  }

  /**
   * Set Error URL.
   *
   * @param string $error_url
   *   Error URL.
   */
  public function setErrorUrl(string $error_url) {
    $this->errorUrl = $error_url;
  }

  /**
   * Set language code.
   *
   * @param string $language_code
   *   Language Code.
   */
  public function setLanguage(string $language_code) {
    $this->language = self::$languageMapping[$language_code] ?? 'AR';
  }

  /**
   * Perform payment initialization and return redirect url.
   *
   * @return string
   *   KNET url to redirect to.
   *
   * @throws \Exception
   */
  public function performPaymentInitialization() {
    // If id, password, key or url is not available, then don't process
    // further.
    if (empty($this->tranportalId)
      || empty($this->tranportalPassword)
      || empty($this->terminalResourceKey)
      || empty($this->knetUrl)) {
      throw new \Exception('K-Net required parameters not available.');
    }

    $trandata = 'id=' . $this->tranportalId;
    $trandata .= '&password=' . $this->tranportalPassword;

    if ($this->amount) {
      $trandata .= '&amt=' . $this->amount;
    }

    if ($this->trackId) {
      $trandata .= '&trackid=' . $this->trackId;
    }

    if ($this->currencyCode) {
      $trandata .= '&currencycode=' . $this->currencyCode;
    }

    if ($this->language) {
      $trandata .= '&langid=' . $this->language;
    }

    if ($this->action) {
      $trandata .= '&action=' . $this->action;
    }

    if ($this->responseUrl) {
      $trandata .= '&responseURL=' . $this->responseUrl;
    }

    if ($this->errorUrl) {
      $trandata .= '&errorURL=' . $this->errorUrl;
    }

    if ($this->udf1) {
      $trandata .= '&udf1=' . $this->udf1;
    }

    if ($this->udf2) {
      $trandata .= '&udf2=' . $this->udf2;
    }

    if ($this->udf3) {
      $trandata .= '&udf3=' . $this->udf3;
    }

    if ($this->udf4) {
      $trandata .= '&udf4=' . $this->udf4;
    }

    if ($this->udf5) {
      $trandata .= '&udf5=' . $this->udf5;
    }

    // Encrypt the request url.
    $trandata = SecureText::encrypt($trandata, $this->terminalResourceKey);
    if (strlen($trandata) === 0) {
      throw new \RuntimeException('Transaction data encryption failed.');
    }

    $url = $this->knetUrl . '?param=paymentInit';
    $url .= '&trandata=' . $trandata;
    $url .= '&tranportalId=' . $this->tranportalId;
    $url .= '&responseURL=' . $this->responseUrl;
    $url .= '&errorURL=' . $this->errorUrl;

    return $url;
  }

}
