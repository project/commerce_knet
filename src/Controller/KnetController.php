<?php

namespace Drupal\commerce_knet\Controller;

use Drupal\commerce_knet\Helper\SecureText;
use Drupal\commerce_knet\Plugin\Commerce\PaymentGateway\KnetInterface;
use Drupal\commerce_payment\Controller\PaymentCheckoutController;
use Drupal\commerce_payment\Entity\Payment;
use Drupal\commerce_price\Price;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Access\AccessException;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Handle KNET return urls.
 */
class KnetController extends PaymentCheckoutController {

  use StringTranslationTrait;

  /**
   * Payment Gateway configuration.
   *
   * @var array
   */
  protected $configuration;

  /**
   * Provides the "return" checkout payment page.
   *
   * Redirects to the next checkout page, completing checkout.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   */
  public function returnPage(Request $request,
                             RouteMatchInterface $route_match) {
    $data = $request->request->all();

    if (empty($data)) {
      $this->logger->error('KNET: No data in POST found in response page.');
      throw new AccessDeniedHttpException();
    }

    /** @var \Drupal\commerce_order\Entity\Order $order */
    $order = $route_match->getParameter('commerce_order');

    $step_id = $route_match->getParameter('step');
    $this->validateStepId($step_id, $order);
    $payment_gateway = $order->get('payment_gateway')->entity;
    $payment_gateway_plugin = $payment_gateway->getPlugin();
    if (!$payment_gateway_plugin instanceof KnetInterface) {
      throw new AccessException('The payment gateway for the order does not implement ' . KnetInterface::class);
    }

    $this->configuration = $payment_gateway_plugin->getConfiguration();

    try {
      $data = $this->parseAndPrepareKnetData($data);
    }
    catch (\Exception $e) {
      $this->logger->error('KNET: Error occured while parsing data. Data: @data, Exception: @message', [
        '@data' => json_encode($data),
        '@message' => $e->getMessage(),
      ]);

      throw new AccessDeniedHttpException();
    }

    $order_id = $data['udf3'] ?? NULL;
    if (empty($order_id)) {
      $this->logger->error('KNET: Invalid response call found.<br>POST: @message', [
        '@message' => json_encode($data),
      ]);

      return $this->cancelPage($request, $route_match);
    }

    if ($data['result'] !== 'CAPTURED') {
      $this->logger->error('KNET: result is not captured, transaction failed. POST: @message', [
        '@message' => json_encode($data),
      ]);

      return $this->cancelPage($request, $route_match);
    }

    if ($data['udf3'] != $order->id()) {
      $this->logger->error('KNET: Response data do not match for order id. POST: @message', [
        '@message' => json_encode($data),
      ]);

      return $this->cancelPage($request, $route_match);
    }

    if ($data['amt'] != $order->getTotalPrice()->getNumber()) {
      $this->logger->error('KNET: Amount currently in order do not match amount in response. Order amount: @order_amount, POST: @message', [
        '@message' => json_encode($data),
        '@order_amount' => $order->getTotalPrice()->getNumber(),
      ]);

      return $this->cancelPage($request, $route_match);
    }

    $this->logger->info('KNET: Payment complete for @order_id with unique token @token. POST: @message', [
      '@order_id' => $data['udf3'],
      '@message' => json_encode($data),
      '@token' => $data['udf4'],
    ]);

    /** @var \Drupal\commerce_payment\Entity\Payment $payment */
    $payment = Payment::create([
      'type' => 'payment_default',
      'payment_gateway' => $payment_gateway->id(),
      'payment_gateway_mode' => $this->configuration['mode'],
      'order_id' => $order->id(),
      'amount' => new Price(strval($data['amt']), $order->getTotalPrice()->getCurrencyCode()),
      'remote_id' => $data['auth'] ?? $data['tranid'],
      'remote_state' => $data['result'],
      'state' => 'completed',
    ]);

    $payment->setAdditionalData($data);
    $payment->save();

    $checkout_flow = $order->get('checkout_flow')->entity;
    $checkout_flow_plugin = $checkout_flow->getPlugin();
    $redirect_step_id = $checkout_flow_plugin->getNextStepId($step_id);

    return $checkout_flow_plugin->redirectToStep($redirect_step_id);
  }

  /**
   * Provides the "cancel" checkout payment page.
   *
   * Redirects to the previous checkout page.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   */
  public function cancelPage(Request $request,
                             RouteMatchInterface $route_match) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $route_match->getParameter('commerce_order');
    $step_id = $route_match->getParameter('step');
    $this->validateStepId($step_id, $order);

    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
    $payment_gateway = $order->get('payment_gateway')->entity;
    $payment_gateway_plugin = $payment_gateway->getPlugin();
    if (!$payment_gateway_plugin instanceof KnetInterface) {
      throw new AccessException('The payment gateway for the order does not implement ' . KnetInterface::class);
    }

    /** @var \Drupal\commerce_checkout\Entity\CheckoutFlowInterface $checkout_flow */
    $checkout_flow = $order->get('checkout_flow')->entity;
    $checkout_flow_plugin = $checkout_flow->getPlugin();

    $data = $request->request->all();
    if (!empty($data)) {
      $data = $this->parseAndPrepareKnetData($data);
    }

    $this->logger->warning('KNET: Payment failed. POST: @message', [
      '@message' => json_encode($data),
    ]);

    $this->messenger->addError($this->t('Payment failed at the payment server. Please review your information and try again.'));
    $this->messenger->addError($this->t('Transaction ID: @transaction_id, Payment ID: @payment_id, Status: @status', [
      '@transaction_id' => $data['tranid'] ?? '',
      '@payment_id' => $data['paymentid'] ?? '',
      '@status' => $data['result'],
    ]));

    $previous_step_id = $checkout_flow_plugin->getPreviousStepId($step_id);
    $checkout_flow_plugin->redirectToStep($previous_step_id);
  }

  /**
   * Parse and prepare K-Net response data for new toolkit.
   *
   * @param array $input
   *   Data to parse.
   *
   * @return array
   *   Data to return after parse.
   *
   * @throws \Exception
   */
  protected function parseAndPrepareKnetData(array $input) {
    // If error is available.
    if (!empty($input['ErrorText']) || !empty($input['Error'])) {
      $this->logger->error('KNET: Response contains Error: @error', [
        '@error' => json_encode($input),
      ]);

      return $input;
    }

    $terminal_resource_key = $this->configuration['terminal_resource_key'];

    // Decrypted data contains a string which seperates values by `&`, so we
    // need to explode this. Example - 'paymentId=123&amt=4545'.
    $output = [];
    $decrypted_data = array_filter(explode('&', SecureText::decrypt($input['trandata'], $terminal_resource_key)));
    array_walk($decrypted_data, function ($val) use (&$output) {
      $key_value = explode('=', $val);
      $output[$key_value[0]] = $key_value[1];
    });

    // Decode udf2.
    // To allow + addressing in mail we encode it before sending to KNET.
    if (isset($output['udf2'])) {
      $output['udf2'] = base64_decode($output['udf2']);
    }

    return $output;
  }

}
