<?php

namespace Drupal\commerce_knet\PluginForm;

use Drupal\commerce_knet\Toolkit\KnetToolKit;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class KnetPaymentOffsiteForm.
 *
 * @package Drupal\commerce_knet\PluginForm
 */
class KnetPaymentOffsiteForm extends PaymentOffsiteForm implements ContainerInjectionInterface {

  use LoggerChannelTrait;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('language_manager'),
      $container->get('current_user'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * KnetPaymentOffsiteForm constructor.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(LanguageManagerInterface $language_manager,
                              AccountInterface $account,
                              EntityTypeManagerInterface $entity_type_manager) {
    $this->languageManager = $language_manager;
    $this->currentUser = $account;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    $this->order = $payment->getOrder();

    $url_options = [
      'commerce_order' => $this->order->id(),
      'step' => 'payment',
    ];
    $form['#return_url'] = Url::fromRoute('commerce_knet.checkout.return', $url_options)->setAbsolute()->toString();
    $form['#cancel_url'] = Url::fromRoute('commerce_knet.checkout.cancel', $url_options)->setAbsolute()->toString();

    $unique_token = uniqid($this->getOrderNumber() . '.');

    $this->getLogger('knet')->notice('KNET: Attempting payment with unique token, @token', [
      '@token' => $unique_token,
    ]);

    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $configuration = $payment->getPaymentGateway()->getPlugin()->getConfiguration();

    $toolkit = new KnetToolKit();

    $toolkit->setKnetUrl($configuration['endpoint_' . $configuration['mode']]);
    $toolkit->setTranportalId($configuration['tranportal_id']);
    $toolkit->setTranportalPassword($configuration['tranportal_password']);
    $toolkit->setTerminalResourceKey($configuration['terminal_resource_key']);

    $toolkit->setResponseUrl($form['#return_url']);
    $toolkit->setErrorUrl($form['#cancel_url']);
    $toolkit->setLanguage($this->languageManager->getCurrentLanguage()->getId());

    $toolkit->setTrackId($this->order->getOrderNumber());
    $toolkit->setAmount($payment->getAmount()->getNumber());
    $toolkit->setUdf1($this->replaceToken('[user_id]'));
    // To allow + addressing in mail we encode it before sending to KNET.
    $toolkit->setUdf2(base64_encode($this->replaceToken('[user_mail]')));
    $toolkit->setUdf3($this->replaceToken('[order_id]'));
    $toolkit->setUdf4($unique_token);
    $toolkit->setUdf5($this->replaceToken($configuration['udf5']));

    $redirect_url = $toolkit->performPaymentInitialization();

    $this->getLogger('knet')->notice('KNET: Redirecting user for payment with unique token, @token', [
      '@token' => $unique_token,
    ]);

    $data = [
      'return' => $form['#return_url'],
      'cancel' => $form['#cancel_url'],
      'total' => $payment->getAmount()->getNumber(),
    ];

    $form = $this->buildRedirectForm($form, $form_state, $redirect_url, $data, 'GET');

    return $form;
  }

  /**
   * Replace token for UDF data.
   *
   * @param string $data
   *   UDF Data with token.
   *
   * @return string
   *   UDF Data with dynamic value.
   */
  protected function replaceToken(string $data) {
    $data = trim($data);
    if (empty($data)) {
      return '';
    }

    if (strpos($data, '[user_id]') !== FALSE) {
      $uid = $this->currentUser->isAnonymous()
        ? '0'
        : $this->currentUser->id();
      $data = str_replace('[user_id]', $uid, $data);
    }

    if (strpos($data, '[user_mail]') !== FALSE) {
      $mail = $this->currentUser->isAnonymous()
        ? $this->order->getEmail()
        : $this->currentUser->getEmail();
      $data = str_replace('[user_mail]', $mail, $data);
    }

    if (strpos($data, '[order_id]') !== FALSE) {
      $data = str_replace('[order_id]', $this->order->id(), $data);
    }

    if (strpos($data, '[order_number]') !== FALSE) {
      $data = str_replace('[order_number]', $this->getOrderNumber(), $data);
    }

    if (strpos($data, '[langcode]') !== FALSE) {
      $data = str_replace('[langcode]', $this->languageManager->getCurrentLanguage()->getId(), $data);
    }

    return (string) $data;
  }

  /**
   * Get the generated order number.
   *
   * Sets the value if not already set.
   *
   * @return string
   *   Generated order number.
   */
  protected function getOrderNumber() {
    if (!$this->order->getOrderNumber()) {
      $order_type_storage = $this->entityTypeManager->getStorage('commerce_order_type');

      /** @var \Drupal\commerce_order\Entity\OrderTypeInterface $order_type */
      $order_type = $order_type_storage->load($this->order->bundle());

      /** @var \Drupal\commerce_number_pattern\Entity\NumberPatternInterface $number_pattern */
      $number_pattern = $order_type->getNumberPattern();

      if ($number_pattern) {
        $order_number = $number_pattern->getPlugin()->generate($this->order);
      }
      else {
        $order_number = 'Order number: ' . $this->order->id();
      }

      $this->order->setOrderNumber($order_number);
      $this->order->save();
    }

    return $this->order->getOrderNumber();
  }

}
